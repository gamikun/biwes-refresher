from datetime import datetime


class Channel(object):
    NOTIFICATIONS = 1
    CHAT = 2

    PLAIN_MESSAGE = 1
    JSON = 2

    __slots__ = ['clients', 'masters', 'master_key',
                 'id', 'api_id', 'mode', 'date_created']

    def __init__(self, mode=NOTIFICATIONS, protocol=JSON,
                       master_key="xxx", dictionary=None):
        self.mode = mode
        self.clients = []
        self.master_key = master_key
        self.date_created = datetime.now()

    def close_all(self):
        for client in self.clients:
            try:
                client.close()
            except:
                pass

class RefresherError(Exception): pass