import tornado.ioloop
import tornado.web
import tornado.websocket
import os
import sys
import data
import simplejson as json
from apierrors import *
from tornado.httpclient import HTTPResponse
from binascii import hexlify
from models import *
from factory import initial_channels


CURDIR = os.path.abspath(os.path.dirname(__file__))
STORE_DATA = True if not '--no-data' in sys.argv else False

class HttpChannelHandler(tornado.web.RequestHandler):
    def get(self): self.response_404()

    def post(self):
        """ Create a new channel """

        api = self.get_api()

        if api is None:
            return self.response_401(INVALID_API)

        # Generate main channel ID and its master
        chan_id = hexlify(os.urandom(16))
        master_key = hexlify(os.urandom(16))

        mode = self.get_argument('mode', 'echo')

        channel = Channel()
        channel.id = chan_id
        channel.master_key = master_key

        if mode == 'echo':
            channel.mode = Channel.ECHO
        else:
            channel.mode = Channel.NOTIFICATIONS

        self.application.channels[chan_id] = channel

        # send channel to databae
        if STORE_DATA:
            data.DB().makechan(channel)

        print("Created channel: %s (%s)" % (channel.id, mode,))

        return self.response(channel_id=chan_id,
                             master_key=master_key)

    def delete(self):
        """ Deletes a channel """

        api = self.get_api()

        if api is None:
            return self.response_401(INVALID_API)

        channel_id = self.get_argument('channel_id', None)
        channel = self.application.channels.get(channel_id, None)

        if channel is None:
            return self.response_404(INVALID_CHANNEL)

        # TODO: deleter must be owner or have the master id
        channel.close_all()
        del self.application.channels[channel_id]
        # TODO: add to a history database

        return self.response(channel_id=channel_id)


    def put(self, chanid=None):
        """ Receive data from the notifier """

        print "Receiving an update"

        api = self.get_api()

        if api is None:
            return self.response_401(INVALID_API)
            print "Invalid API"

        channel = self.get_channel(id=chanid)

        if not channel:
            print "Channel doesn't exists"
            return self.response_404(error=INVALID_CHANNEL)

        print channel.mode
        if channel.mode == Channel.ECHO:
            data2send = self.get_argument('data', None)
            for client in channel.clients:
                client.write_message(data2send)

        else: # TODO: add definition for other modes
            msg = self.get_argument('msg', None)
            if msg is None or msg == '':
                print "empty message"
                return self.response(error=EMPTY_MESSAGE,
                                     http_status=403)
            data2send = {'op': 'msg', 'body': msg}
            msg2send = json.dumps(data2send)
            for client in channel.clients:
                client.write_message(msg2send)

        return self.response()

    def response(self, error=None, http_status=200,
                 error_message=None, **kwargs):
        self.set_status(http_status)
        response = {}

        if not error is None:
            response['error_code'] = error
        if error_message is not None:
            response['error_message'] = error_message

        response.update(kwargs)
        self.write(json.dumps(response))
        return None

    def get_api(self):
        if not STORE_DATA: return {}
        if not 'Authorization' in self.request.headers:
            return None
        auth = self.request.headers.get('Authorization', '').split()
        if auth is None or len(auth) < 2:
            return None
        db = data.DB()
        api = db.get_api(id=auth[0], secret=auth[1])
        return api

    def get_channel(self, id=None):
        return self.application.channels.get(id, None)

    def response_401(self, error=None, **kwargs):
        return self.response(http_status=401,
                             error=error, **kwargs)

    def response_404(self, error=None, **kwargs):
        return self.response(http_status=404,
                             error=error, **kwargs)

    """
    def options(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Methods', 'PUT,POST,OPTIONS')
        self.set_header('Access-Control-Max-Age', '1800')
    """


class WsHandler(tornado.websocket.WebSocketHandler):
    opcodes = ['msg', 'kick']
    def __init__(self, a, b):
        super(WsHandler, self   ).__init__(a, b)
        self.alive = True # determines whether the socket
                          # is still usable or is queued to
                          # be removed later
        self.channel = None # This channel is always the same
                            # from begin to end.

    def open(self, chanid=None, apikey=None):
        self.channel = self.get_channel(chanid)
        # No communication is permitted with no channel
        if self.channel is None:
            print "channel doesn't exists"
            return self.close()

        self.channel.clients.append(self)

    def on_message(self, data):
        # When in a notification channel and client
        # tries to send a message, connection will be closed.
        if self.channel.mode == Channel.NOTIFICATIONS:
            return self.close()

        # Echo channels are easier to send data,
        # it will only send the same packet to everyone.
        if self.channel.mode == Channel.ECHO:
            for client in self.channel.clients:
                client.write(data)
            return

        try:
            # When channel is json, need to be parsed
            if self.channel.protocol == Channel.JSON:
                message = json.loads(data)
                # Read the command type
                opcode = message.get('op', None)
                if opcode is None or not opcode in self.opcodes:
                    raise RefresherError()

                if opcode == 'msg':
                    body = message.get('body', None)
                    if body is None:
                        raise RefresherError()
                    for client in self.channel.clients:
                        if client == self: continue
                        msg2send = {'op': 'msg', 'body': body}
                        client.write_message(json.dumps(msg2send))

            else:
                raise NotImplementedError()            

        except JSONDecodeError:
            return self.close()

        except RefresherError:
            return self.close()

        except:
            return self.close()

    def on_close(self):
        self.channel.clients.remove(self)
        # TODO: make it softer removing it

    def get_channel(self, channel_id=None):
        if channel_id is None:
            return None
        return self.application.channels.get(channel_id, None)

    def check_origin(self, origin):
        return True

class PingPongHandler(tornado.web.RequestHandler):
    def get(self):
        self.write('pong')


urls = [
    # Websocket client
    (r"^/channel/(?P<chanid>[a-z0-9]+)/join$", WsHandler),

    # Channel generator (common webserver)
    (r"^/channel$", HttpChannelHandler),

    # Channel generator (common webserver)
    (r"^/channel/(?P<chanid>[a-z0-9]{32})$", HttpChannelHandler),

    # Big data processor
    (r"^/channel/join/(?P<chanid>[a-z0-9]{32})/(?P<masterkey>[a-z0-9]{48})$", WsHandler),

    # Ping Pong
    (r"^/ping$", PingPongHandler),
]

app = tornado.web.Application(urls)
app.channels = initial_channels() if STORE_DATA else {}
print app.channels

print "__name__: %s" % __name__

if __name__ == "__main__":
    try:
        port = 8290
        print "Listening %d..." % port
        app.listen(port)
        tornado.ioloop.IOLoop.instance().start()
    except KeyboardInterrupt:
        tornado.ioloop.IOLoop.instance().stop()
        print "\nBye"