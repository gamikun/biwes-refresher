from cassandra.cluster import Cluster
from cassandra.query import dict_factory
from binascii import hexlify, unhexlify
from datetime import datetime
import hashlib
import os
import time
import re
from models import *


class DB(object):
    re_hash32 = re.compile('[a-f0-9]{32}')
    re_id = re.compile('[a-f0-9]{32}')
    is_invalid_hash32 = lambda x: re_hash32.match(x) is None
    db_name = None
    
    def __init__(self):
        if self.db_name is None:
            self.db_name = os.environ.get('db_name', 'biwes_refresher')
        self.cluster = Cluster(['127.0.0.1'])
        self.session = self.cluster.connect(self.db_name)
        #self.session.row_factory = dict_factory

    def get_api(self, id=None, secret=None):
        """ Query the api in the database """

        if id is None or not self.re_id.match(id):
            return None
        if secret is None or len(secret) != 48:
            return None

        result = self.session.execute(
            "SELECT id FROM api " + \
            "WHERE id = %s AND secret = %s LIMIT 1",
            (bytearray(unhexlify(id)), secret, )
        )

        if len(result) == 0:
            return None

        return result[0]

    def getchan(self, id):
        result = self.session.execute(
            "SELECT * FROM channel WHERE id = %s LIMIT 1",
            (bytearray(unhexlify(id)), )
        )
        return result[0] if len(result) > 0 else None

    def makechan(self, chan):
        self.session.execute(
            "INSERT INTO channel(" +
                "id, master_key, created," +
                "message_count, client_count," +
                "mode, is_alive)" +
            "VALUES(%s, %s, %s, 0, 0, 1, true)",
            (bytearray(unhexlify(chan.id)),
             chan.master_key,
             chan.date_created
            )
        )

    def getchans(self, alive=True):
        return self.session.execute(
            "SELECT * FROM channel " +
            "WHERE is_alive = true"
        )

