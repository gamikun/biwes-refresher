from models import Channel
from data import DB
from binascii import hexlify


def initial_channels():
    channels = {}
    for c in DB().getchans():
        chan = Channel(
            mode=c.mode,
            master_key=c.master_key,
        )
        chan.id = hexlify(c.id)
        channels[chan.id] = chan
    return channels