import unittest
import requests
import simplejson as json


class TestSender(unittest.TestCase):
    def setUp(self):
        self.test_server = 'http://localhost:8290'
        self.api_key = "3cd2c7a221bc7c179390fdf453663b3d"
        self.api_secret = "SJijidDihfoihjfoihfo55klfnfa+sjXidof-_fXdGtXtO04"
        self.channel_id = "a1928a17f5c5da485635a58c12d0f864"

    def test_create_channel(self):
        response = requests.post(
            url=self.test_server + '/channel',
            headers={
                "Content-Type": "application/json",
                "Authorization": "%s %s"
                    % (self.api_key, self.api_secret)
            }
        )

        self.assertTrue(response.status_code == 200,
                        "Invalid status code %d"
                        % response.status_code )
        self.assertTrue(response.content != "",
                        "Empty response %s" % response.content)

        result = json.loads(response.content)
        self.assertTrue(not result is None, "Invalid JSON response")
        self.assertTrue(result.get("channel_id", False),
                        "No channel ID received")
        self.assertTrue(result.get("master_id", False),
                        "No mater ID received")
        print "Created Channel: %s" % result.get('channel_id')

    def test_send_message(self):
        response = requests.put(
            url=self.test_server + '/channel',
            headers={
                "Content-Type": "application/json",
                "Authorization": "%s %s"
                    % (self.api_key, self.api_secret),
            },
            params={'channel_id': self.channel_id}
        )
        self.assertTrue(response.status_code == 200, "Status code %d"
                        % response.status_code)

    def make_and_get_channel(self):
        response = requests.post(
            url=self.test_server + '/channel',
            headers={
                "Content-Type": "application/json",
                "Authorization": "%s %s"
                    % (self.api_key, self.api_secret)
            }
        )
        if response.status_code != 200: return None
        if response.content == "": return None
        data = json.loads(response.content)
        if data is None: return None

        return data.get("channel_id")


if __name__ == "__main__":
    unittest.main()