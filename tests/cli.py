import requests
import simplejson as json
import re

base_url = 'http://localhost:8290'
api_key = '3cd2c7a221bc7c179390fdf453663b3d'
api_secret = 'SJijidDihfoihjfoihfo55klfnfa+sjXidof-_fXdGtXtO04'
channel_id = None
last_command = None

re_repeat = re.compile('\*(\d+)')

def create_channel():
    r = requests.post(
        url=base_url + '/channel',
        headers={
            "Content-Type": "application/json",
            "Authorization": "%s %s"
                % (api_key, api_secret)
        }
    )
    try:
        data = json.loads(r.content)
        return data.get('channel_id')
    except:
        pass

def send(channel_id, msg):
    r = requests.put(
        url=base_url + '/channel',
        headers={
            "Content-Type": "application/json",
            "Authorization": "%s %s"
                % (api_key, api_secret)
        },
        params={"channel_id": channel_id, "msg": msg}
    )
    print channel_id
    print r.content

def delete_channel(channel_id):
    r = requests.delete(
        url=base_url + '/channel',
        headers={
            "Content-Type": "application/json",
            "Authorization": "%s %s"
                % (api_key, api_secret)
        },
        params={"channel_id": channel_id}
    )
    print r.content

try:
    while True:
        cmd = raw_input("> ")

        if cmd == '*':
            cmd = last_command

        if cmd.startswith("makechan"):
            channel_id = create_channel()
            print channel_id
        elif cmd.startswith("send"):
            p = cmd.split(" ", 1)
            if len(p) < 2:
                print "No message given"
                continue
            send(channel_id, p[1])
        elif cmd.startswith("delchan"):
            delete_channel(channel_id)
            channel_id = None
        else:
            print "Invalid command"
        last_command = cmd
except KeyboardInterrupt:
    print "\nbye"